import $ from 'jquery';
import 'bootstrap';
import 'daterangepicker';
import 'google-maps';

/**
 * Styles
 */
import "../scss/index.scss";

/**
 * Modules
 */
import { silcCoreInit } from 'silc-core';
import { silcAccordionInit } from 'silc-accordion';
import { silcNavInit } from 'silc-nav';
import { silcOffcanvasInit } from 'silc-offcanvas';
import { menu } from '../components/sections/s-navegation/navegation';
import { skills } from '../components/comp/boards/boards';



/**
 * Init
 */

skills();
menu();
silcCoreInit();
silcAccordionInit();
silcNavInit();
silcOffcanvasInit();



