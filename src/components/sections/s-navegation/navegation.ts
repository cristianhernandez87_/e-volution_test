export function menu() {

    if(document.getElementById("menu-open")!=null){
		document.getElementById("menu-open").addEventListener("click", function() {
	        moveMenu();
	    });    
    }
    
    
    function moveMenu() {
        if (document.body.classList.contains('open-menu')) {
            document.body.classList.remove('open-menu');
        } else {
            document.body.classList.add('open-menu');
        };
    }
	if(document.getElementById("booking_action")!=null){
	    document.getElementById("booking_action").addEventListener("click", function() {
	        book_window();
	    });
    }
    
    function book_window() {
        if (document.body.classList.contains('open-booking')) {
            document.body.classList.remove('open-booking');
        } else {
            document.body.classList.add('open-booking');
        };
    }

    $(window).scroll(function(event) {
        var altura = $(window).height();
        var scroll = $(window).scrollTop();
        var alturaNecesitada = altura - 800;
        // console.log(alturaNecesitada);
        // console.log(altura);
        if (scroll >= alturaNecesitada) {
            $("body").addClass("scroll-menu");
            //console.log(altura);
        } else {
            $("body").removeClass("scroll-menu");
        }
    });  
}